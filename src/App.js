import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import LocationSelection from './containers/Main';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container className="spacer">
          <Row>
            <Col sm="12" md={{ size: 6 }}>
              <h3>Cheese Curd Crawl 2019 </h3>
            </Col>
          </Row>
          <Row className="spacer">
            <Col>
              <LocationSelection />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
