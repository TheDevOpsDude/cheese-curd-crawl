import React, { useState } from 'react';
import {
  Row,
  Col,
  Form,
  Button,
  ButtonGroup,
  FormGroup,
  Input,
  Label
} from 'reactstrap';

function handleSubmit(event) {
  console.log('idk do somehting');
}

export default function VotingForm(props) {
  return (
    <Form onSubmit={handleSubmit}>
      <Row className="spacer">
        <Col xs={{ size: 12 }}>
          <FormGroup>
            <Label for="exampleSelect">Overall Rating</Label>
            <Input type="select" name="select" id="exampleSelect">
              <option value="" selected disabled>
                Please select
              </option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>

      <Row className="spacer">
        <Col xs={{ size: 12 }}>
          <FormGroup>
            <Label for="exampleSelect">Grease</Label>
            <Input type="select" name="select" id="exampleSelect">
              <option value="" selected disabled>
                Please select
              </option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <Row className="spacer">
        <Col xs={{ size: 12 }}>
          <FormGroup>
            <Label for="exampleSelect">Cheese</Label>
            <Input type="select" name="select" id="exampleSelect">
              <option value="" selected disabled>
                Please select
              </option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>

      <Row className="spacer">
        <Col xs={{ size: 12 }}>
          <FormGroup>
            <Label for="exampleSelect">Breading</Label>
            <Input type="select" name="select" id="exampleSelect">
              <option value="" selected disabled>
                Please select
              </option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>

      <Row className="spacer">
        <Col xs={{ size: 12 }}>
          <Button type="submit">Submit</Button>
        </Col>
      </Row>
    </Form>
  );
}
