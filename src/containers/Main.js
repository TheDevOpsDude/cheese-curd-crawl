import React, { useState } from 'react';
import {
  Col,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import VotingForm from './Voting';

export default function LocationSelection(props) {
  const [dropdownState, setdropdownState] = useState(false);
  const [dropdownValueState, setDropdownValueState] = useState('Select a Stop');

  function toggle() {
    setdropdownState(dropdownState === false ? true : false);
  }

  function handleChange(e) {
    setDropdownValueState(e.target.innerText);
  }

  return (
    <Col xs={{ size: 12 }}>
      <ButtonDropdown
        isOpen={dropdownState}
        toggle={toggle}
        onChange={handleChange}
        value={dropdownValueState}
      >
        <DropdownToggle caret color="info" text="hello" onChange={handleChange}>
          {dropdownValueState}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={handleChange}>
            Milwaukee Brewing Company
          </DropdownItem>
          <DropdownItem onClick={handleChange}> The Rumpus Room</DropdownItem>
          <DropdownItem onClick={handleChange}>
            Harley-Davidson Museum
          </DropdownItem>
          <DropdownItem onClick={handleChange}>Fuel Cafe 5th St</DropdownItem>
          <DropdownItem onClick={handleChange}>Black Sheep MKE</DropdownItem>
          <DropdownItem onClick={handleChange}>The Outsider</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>

      <VotingForm />
    </Col>
  );
}
